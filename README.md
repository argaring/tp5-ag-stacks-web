# TP5 - Créations de stacks web

Bonjour, ce repo est réalisé pour le TP4 du cour "INFAL103-Essentiels de l'infrastructure du SI" à CESI

## Énoncé

Étant donné 3 applications web dans les langages PHP, Javascript (avec NodeJS) et Python (avec Flask), créez pour
chacune, une page affichant le message “Bonjour ! Nous sommes le jj/mm/aaaa et il est hh:ss” où jj/mm/aaaa correspondra
à la date courante et hh:ss à l’heure courante.

## Livrables

Pour chaque stack, le livrable sera une URL vers un dépôt git. Ce dépôt git devra se trouver sur gitlab et être partagé
avec mon user (@gknb). Ce dépôt devra également contenir les éléments suivants a minima :

- Un document README.md documentant l'organisation du repository et pointant vers les éventuels sous-dossiers.
- Un sous-dossier pour chaque stack avec les éléments nécessaires pour la mise en œuvre.
- un document nommé Deployment.md dans chaque sous-dossier de stack détaillant la procédure de mise en œuvre (
  déploiement) de l’applicatif étape par étape avec tous les éléments et pré-requis nécessaires. Le suivi de la
  documentation devra permettre de déployer l'application dans un environnement local ou distant et de la rendre
  disponible à une certaine URL.

## Les stacks

Ici on a une description des différents stacks implémenté.

|     Nom      | Chemin         | Description                                                                     |
|:------------:|:---------------|:--------------------------------------------------------------------------------|
|    Simple    | `/Simple`      | Stack simple utilisant l'image httpd, un fichier index.html et le script app.js |
|     LAMP     | `/LAMP`        | Stack LAMP utilisant l'image **php:8.2-apache** et fichier index.php            |
| Python Flask | `/PythonFlask` | Stack Python Flask, utilisant l'image docker **python:3.11**                    |
