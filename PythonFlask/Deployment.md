# Deployement pour le stack "PythonFlask"

## Description du stack

Stack réalisé avec l'image **python:3.11** et le fichier flask_app.py

## Étapes de déployment

1. Lancer la commande : `docker compose up`
2. Ouvril la page web : http://localhost