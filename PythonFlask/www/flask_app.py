from flask import Flask
from datetime import datetime
from dateutil import tz

app = Flask(__name__)


@app.route('/')
def hello():
    timezone = tz.gettz('Europe/Paris')
    now = datetime.now(tz=timezone)
    date = now.strftime("%d/%m/%Y")
    hour = now.strftime("%H:%M:%S")

    return f'Bonjour <br>' \
           f'Nous somme le {date} et il est {hour}'


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=True)
