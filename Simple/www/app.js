const zeroPad = (num, places) => String(num).padStart(places, '0')

function setTime() {
    let date =  new Date();

    let day = `${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`;
    let time = `${date.getHours()}:${zeroPad(date.getMinutes(), 2)}:${zeroPad(date.getSeconds(), 2)}`;

    let dayEl = document.querySelector('#date');
    let timeEl = document.querySelector('#time');

    dayEl.innerHTML = day;
    timeEl.innerHTML = time;
}

window.addEventListener('load', setTime);

