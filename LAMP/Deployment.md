# Deployement pour le stack "LAMP"

## Description du stack

Stack réalisé avec l'image **php:8.0-apache** et le fichier index.php

## Étapes de déployment

1. Lancer la commande : `docker compose up`
2. Ouvril la page web : http://localhost